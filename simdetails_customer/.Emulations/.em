<?xml version="1.0" encoding="ASCII"?>
<emulation:EmulationData xmlns:emulation="http:///emulation.ecore" isBW="true" location="simdetails_customer">
  <ProcessNode Id="simdetails_customer.QueryCustomerDetails" Name="simdetails_customer.QueryCustomerDetails" ModelType="BW" moduleName="simdetails_customer">
    <Operation Name="callProcess" serviceName="simdetails_customer.QueryCustomerDetails">
      <Inputs Id="ecfdb4ac-3611-47b5-a90a-51991a8a2dccsimdetails_customer_simdetails_customer.QueryCustomerDetails_callProcess_Start" Name="Start" isDefault="true" type="CALLPROCESS">
        <informations Name="input" tnsName="tns:" nameSpace="xmlns:tns">
          <nameSpaces prefix="tns" nameSapce="http://www.example.org/InputSchema"/>
          <Parameter Name="tns:Input_Number" Value="&#xA;  ">
            <parameters Name="tns:GSM_Number" Value="9500060000" type="integer" nameSpace="http://www.example.org/InputSchema"/>
          </Parameter>
        </informations>
      </Inputs>
    </Operation>
  </ProcessNode>
  <ProcessNode Id="simdetails_customer.Resource" Name="simdetails_customer.Resource" ModelType="BW" moduleName="simdetails_customer">
    <Operation Name="get" serviceName="getdetails-customer">
      <Inputs Id="efb43abf-cd26-44e6-b0a5-db87f6f37fd5simdetails_customer_simdetails_customer.Resource_get_getInput" Name="getInput" isDefault="true"/>
    </Operation>
  </ProcessNode>
</emulation:EmulationData>
